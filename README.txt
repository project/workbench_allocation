CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers



INTRODUCTION
------------
The Workbench Allocation module allows user to assign content for review to 
specific users or groups of users. A user is placed into a group via a 
taxonomy term attached to their account. At present only one taxonomy term 
is permitted per user. When a user moves a content from Draft to Review a list
of valid reviewers will appear and you have a choice of selecting an individual 
user, a number of users, a full group or no one.
 
 * For a full description of the module, visit the project page:
   https://drupal.org/project/workbench_allocation


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/workbench_allocation



REQUIREMENTS
------------
This module requires the following modules:
 * Workbench (https://www.drupal.org/project/workbench)
 * Workbench Moderation (https://www.drupal.org/project/workbench_moderation)



INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.



CONFIGURATION
-------------
 * You first need to decide if you need a new taxonomy vocabulary or use and 
   existing one to allocate users into groups.

 * Once you have identified the taxonomy vocabulary you need to add it to the
   users' account by going to: 
   Administration » Configuration » People » Account Settings » Manage Fields

 * You then need to select the Content types the workbench allocation list of 
   names should appear on and select the taxonomy vocabulary identified above:
   Administration » Configuration » Workbench » Workbench Moderation »
   Allocation.




MAINTAINERS
-----------
Current maintainers:
 * Stephen Blinkhorne (stephenb001) - https://drupal.org/user/2412300


This project has been sponsored by:
 * Cancer Research UK
