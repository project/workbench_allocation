<?php

/**
 * @file
 * Administrative forms for Workbench Allocation Module.
 */

/**
 * Form for administering the allocation.
 *
 * Administrators can use this form to add, update, delete emails.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 *
 * @return form
 *   Returns the form array.
 */
function workbench_allocation_admin_form(array $form, array &$form_state) {
  $taxonomies = taxonomy_get_vocabularies();
  $options = array(0 => '- None -');
  foreach ($taxonomies as $value) {
    $options[$value->machine_name] = $value->name;
  }

  $form['workbench_allocation_taxonomy'] = array(
    '#type' => 'select',
    '#title' => t('Departments taxonomy'),
    '#options' => $options,
    '#description' => t('Select which taxonomy should be responsable for the allocation departments, users should have a term from this taxonomy to determine their department'),
    '#multiple' => FALSE,
    '#default_value' => variable_get('workbench_allocation_taxonomy', 0),
  );

  // List the content types.
  $form['workbench_allocation_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Content types',
    '#description' => t('Select the content types to use the alocation.'),
    '#options' => workbench_allocation_node_types(),
    '#default_value' => variable_get('workbench_allocation_content_types', array()),
  );

  return system_settings_form($form);
}

/**
 * Get list of node types.
 *
 * Ideally we should only return a list of node types that are setup for
 * moderation, but if we don't have the function call to workbench_moderation
 * then we can just list all node types as default.
 *
 * @return array
 *   List of node types for options.
 */
function workbench_allocation_node_types() {
  // First we try and use the function in workbench moderation to get list
  // of moderated node types.
  if (function_exists('workbench_moderation_moderate_node_types')) {
    // Get list of node types that are set up for moderation.
    $types = drupal_map_assoc(workbench_moderation_moderate_node_types());

    // Get list of all node types.
    $all_types = node_type_get_types();

    // We need to set the name for the node types set up for moderation.
    foreach ($types as $type) {
      $types[$type] = $all_types[$type]->name;
    }
  }
  else {
    $node_types = node_type_get_types();

    // Make sure the name of the node type is human readable.
    foreach ($node_types as $key => $node_type) {
      $types[$key] = $node_type->name;
    }
  }

  asort($types);

  return $types;
}
